const express = require('express');
const app = express();

// Config
const port = 3000;
app.use(express.json());

// Call pages
app.post('/', (req, res) => {
    console.log(req.body.MyKey);      // your JSON
    res.send(req.body.MyKey);    // echo the result back
});

// Start listener
const listener = app.listen(port, function(){
    console.log(`Listening on: http://localhost:${listener.address().port}`); //Listening on port 8888
});