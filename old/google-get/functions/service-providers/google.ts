console.log("GoogleInit works");


// Creates a client
const {Datastore} = require('../node_modules/@google-cloud/datastore');
const datastore = new Datastore();

const kind = 'Product';

module.exports = {
    create: async function createfunction(cID, cProductName, cProductDesc, cProductPrice) {

        // The name/ID for the new entity
        // const name = 'sampletask1';
        const name = cID;

        // The Cloud Datastore key for the new entity
        const taskKey = datastore.key([kind, name]);

        // createdAtTimestamp
        // @ts-ignore
        var Timestamp = Date(Date.now());
        var TimestampString = Timestamp.toString();

        // Prepares the new entity
        const task = {
            key: taskKey,
            data: {
                name: cProductName,
                description: cProductDesc,
                price: cProductPrice,
                created_at: TimestampString,
            },
        };

        // Saves the entity
        await datastore.save(task, (err, entity) => {
            if (err) {
                console.log(`Error: ` + err);
            }
        });
        console.log(`Saved ${task.key.name}: ${task.data.description}`);
    }
}