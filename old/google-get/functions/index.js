const functions = require('firebase-functions');
// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
var fs = require("fs");
const express = require('express');
const app = express();
const cors = require('cors');

exports.initMainApp = functions.https.onRequest((request, response) => {
// export const initMainApp = functions.https.onRequest((request, response) => {
    // function AppInit() {
    const json = fs.readFileSync("config.json");
    const parsedjson = JSON.parse(json);
    const solution = parsedjson.solution;

    const server = app.listen();
    const port = parsedjson.express_port;


    //Start express listener if not started yet
    if (server.listening == false) {
        app.listen(port, () => console.log(`App is listening on port ${port}!`))
        console.log("Server turned (back) on.")
    }
    console.log(`Port used: ${server.listening}`);


    //Get Solution
    if (solution === "google") {
        // require google SDK
        // Imports the Google Cloud client library
        googleInit();
    } else if (solution === "aws") {
        // require aws SDK
        awsInit();
    } else if (solution === "azure") {
        // require azure SDK
        azureInit();
    } else {
        // nothing
        console.error("This solution doesn't exist!")
    }
    // }

    // let initApp = new AppInit();
    // console.log(initApp);

    function googleInit() {
        // https://stackoverflow.com/questions/950087/how-do-i-include-a-javascript-file-in-another-javascript-file

        // https://cloud.google.com/functions/docs/writing/http#parsing_http_requests

        let gs = require('./service-providers/google.ts');
        app.use(cors({ origin: true }));

        app.get('/create/:id', (req, res) => {
            console.log("Reached!");

            res.send(req.params.id);
            console.log(req.params.id);
        });
        exports.widgets = functions.https.onRequest(app);
}


        // exports.google = app.get('/create/:cID', function(req, res) {


        // app.use(cors({ origin: true }))
        // app.get('/create', (req, res) => {
        //     res.send("Hi!!")
        // })
        //
        // const api = functions.https.onRequest(app);
        //
        // module.exports = {
        //     api
        // }
        // exports.googleInit2 = app.get('/create/:cID', function (req, res) {
        //     console.log("Test");
        //     res.send(req.params);
        //     // res.send(req.params);
        //     // res.json(req.params);
        //
        //     // // let name = req.body;
        //     // // let cID = res.send(name).toString();
        //     let cID = req.params.cID;
        //     // // gs.create('A-String-Here2', 'BJURSTA-552', 'IKEA Desk-300', 100);
        //     gs.create(cID, 'BJURSTA-55', 'IKEA Desk-300', 100);
        //     //
        //     // res.send(req.params);
        //     // console.log(cID);
        // });
        // server.close()
        // if (server.listening == false) {
        //     console.log("Server turned off...for now....");
        // }
    // }

    function awsInit() {

    }

    function azureInit() {

    }


});
