var fs = require("fs");
const express = require('express');
const app = express();

function AppInit() {
    const json = fs.readFileSync("config.json");
    const parsedjson = JSON.parse(json);
    let solution = parsedjson.solution;

    this.solution = solution;

    if (solution == "google") {
        // require google SDK
        // Imports the Google Cloud client library
        googleInit();
    } else if (solution == "aws") {
        // require aws SDK
        awsInit();
    } else if (solution == "azure") {
        // require azure SDK
        azureInit();
    } else {
        // nothing
        console.error("This solution doesn't exist!")
    }
}

let initApp = new AppInit();
console.log(initApp);

function googleInit() {
    // https://stackoverflow.com/questions/950087/how-do-i-include-a-javascript-file-in-another-javascript-file

    // https://cloud.google.com/functions/docs/writing/http#parsing_http_requests

    let gs = require('./service-providers/google.ts');
    exports.google = app.get('/create/:cID', (req, res) => {
        let name = req.body;
        // let cID = res.send(name).toString();
        let cID = req.params.cID;
        // gs.create('A-String-Here2', 'BJURSTA-552', 'IKEA Desk-300', 100);
        gs.create(cID, 'BJURSTA-55', 'IKEA Desk-300', 100);
    })
}

function awsInit() {

}

function azureInit() {

}

const port = 3000;
app.listen(port, () => console.log(`App is listening on port ${port}!`))