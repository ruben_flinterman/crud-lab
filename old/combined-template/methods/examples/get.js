const setMethod = 'get';
const method = setMethod.toUpperCase();
const defaultStatus = 200;

module.exports = {
    firebase: async function (msg) {
        try {
            const message = {message: msg, status: defaultStatus, method: method};
            return message;
        } catch (err) {
            return console.error(err);
        }
    },

    aws: async function (msg) {
        try {
            const message = {message: msg, status: defaultStatus, method: method};
            return message;
        } catch (err) {
            return console.error(err);
        }
    },

    azure: async function (msg) {
        try {
            const message = {message: msg, status: defaultStatus, method: method};
            return message;
        } catch (err) {
            return console.error(err);
        }
    }
};