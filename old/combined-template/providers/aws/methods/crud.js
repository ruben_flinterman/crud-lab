// Set default variables for global (in file) use
const defaultStatus = 200;
function setMethod(setMethod) {
    const method = setMethod.toUpperCase();
    return method;
}

// Execute CRUD function when called upon
module.exports = {
    create: async function (msg, solution) {
        const method = setMethod('post');
        try {
            const message = {message: msg, info: { status: defaultStatus, method: method, solutionUsed: solution } };
            return message;
        } catch (err) {
            return console.error(err);
        }
    },

    read: async function (msg, solution) {
        const method = setMethod('get');
        try {
            const message = {message: msg, info: { status: defaultStatus, method: method, solutionUsed: solution } };
            return message;
        } catch (err) {
            return console.error(err);
        }
    },

    update: async function (msg, solution) {
        const method = setMethod('put');
        try {
            const message = {message: msg, info: { status: defaultStatus, method: method, solutionUsed: solution } };
            return message;
        } catch (err) {
            return console.error(err);
        }
    },

    remove: async function (msg, solution) {
        const method = setMethod('delete');
        try {
            const message = {message: msg, info: { status: defaultStatus, method: method, solutionUsed: solution } };
            return message;
        } catch (err) {
            return console.error(err);
        }
    }
};