// Set default variables for global (in file) use
let setStatus = 400;
let msg;
let error;

// Return different error messages when called upon
module.exports = {
    solutionDoesntExist: async function () {
        try {
            msg = 'Solution doesnt exist, please check config.json';
            error = {message: msg, status: setStatus};
            return error;
        } catch (err) {
            return console.error(err);
        }
    },

    noMessageSet: async function() {
        try {
            msg = 'There is no message defined for this method';
            error = {message: msg, status: setStatus};
            return error;
        } catch (err) {
            return console.error(err);
        }
    }
};