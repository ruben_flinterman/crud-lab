// File dependencies.
// npm i express cors path body-parser --save
const express = require('express');
const cors = require('cors');
const fs = require('fs');
const path = require('path');

// Express dependencies
const bodyParser = require('body-parser');

// Config
const rawconfig = fs.readFileSync(path.resolve(__dirname, "./config.json"));
const config = JSON.parse(rawconfig);
const solution = config.solution;
const getSolution = solution.toLowerCase();
const port = config.expressPort;

// Default variable(s)
let result;
let getStatus = 200;
let crud;
let msg = '';

// Init express
const app = express();
app.use(bodyParser.json());
app.use(cors({origin: true}));

// Get files
const error = require(path.resolve(__dirname, './errors/errors.js'));
if (getSolution === 'firebase' || getSolution === 'aws' || getSolution === 'azure') {
    crud = require(path.resolve(__dirname, `./providers/${solution}/methods/crud.js`));
}

// Express functions
app.post('/', async (req, res) => {
    msg = 'Hello!';
    if (msg === '') {
        result = await error.noMessageSet();
        getStatus = 400;
    } else if (getSolution !== 'firebase' && getSolution !== 'aws' && getSolution !== 'azure') {
        result = await error.solutionDoesntExist();
        getStatus = 400;
    } else {
        try {
            result = await crud.create(msg, solution);
        } catch (err) {
            console.error(err);
            getStatus = 400;
        }
    }
    return res.status(getStatus).send(result);
});

app.get('/', async (req, res) => {
    msg = 'Hello!';
    if (msg === '') {
        result = await error.noMessageSet();
        getStatus = 400;
    } else if (getSolution !== 'firebase' && getSolution !== 'aws' && getSolution !== 'azure') {
        result = await error.solutionDoesntExist();
        getStatus = 400;
    } else {
        try {
            result = await crud.read(msg, solution);
        } catch (err) {
            console.error(err);
            getStatus = 400;
        }
    }
    return res.status(getStatus).send(result);
});

app.put('/', async (req, res) => {
    msg = 'Hello!';
    if (msg === '') {
        result = await error.noMessageSet();
        getStatus = 400;
    } else if (getSolution !== 'firebase' && getSolution !== 'aws' && getSolution !== 'azure') {
        result = await error.solutionDoesntExist();
        getStatus = 400;
    } else {
        try {
            result = await crud.update(msg, solution);
        } catch (err) {
            console.error(err);
            getStatus = 400;
        }
    }
    return res.status(getStatus).send(result);
});

app.delete('/', async (req, res) => {
    msg = 'Hello!';
    if (msg === '') {
        result = await error.noMessageSet();
        getStatus = 400;
    } else if (getSolution !== 'firebase' && getSolution !== 'aws' && getSolution !== 'azure') {
        result = await error.solutionDoesntExist();
        getStatus = 400;
    } else {
        try {
            result = await crud.remove(msg, solution);
        } catch (err) {
            console.error(err);
            getStatus = 400;
        }
    }
    return res.status(getStatus).send(result);
});

// Setup express listener
app.listen(port, () => console.log(`Listening on http://localhost:${port}!`));
