//============================================================
// Check on startup.                                        ||
//============================================================
console.log("GoogleInit works");
//============================================================
// Dependencies.                                            ||
//============================================================
const fs = require('fs');
const path = require('path');
const {Datastore} = require('@google-cloud/datastore');
//===============================================================================
// Config.                                                                     ||
//===============================================================================
const rawconfig = fs.readFileSync(path.resolve(__dirname, "../config.json"));
const config = JSON.parse(rawconfig);
//============================================================
// Define database table/input.                             ||
//============================================================
const productTable = config.database.google_input;
//============================================================
// Creates a client.                                        ||
//============================================================
const datastore = new Datastore();
//============================================================
// Sets (Datastore) table for interactions.                 ||
//============================================================
const kind = productTable;
//=================================================================
// Makes every entity an object so it can be called as a JSON    ||
//=================================================================
function entity_to_obj(entity){
    let oKey = entity.key;
    if(!oKey){
        oKey = entity[datastore.KEY];
    }
    if (oKey.id) {
        entity.id = oKey.id;
    }
    if (oKey.name) {
        entity.id = oKey.name;
    }
    return entity;
}
//========================================================================================================================
// createdAtTimestamp.                                                                                                  ||
//========================================================================================================================
const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
const d = new Date();
const Timestamp = d.getDate() + '-' + months[d.getMonth()] + '-' + d.getFullYear() + ' - ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
const TimestampString = Timestamp.toString();
//===========================================================================================================================
// module.exports exports all functions so I can call them in index.js with their own prefixes.                            ||
// All functions in this file will automatically be started as long as their 'alias' (Ex: create) is called in index.js.   ||
//===========================================================================================================================
module.exports = {
    create: async function (cID, cProductName, cProductDesc, cProductPrice) {
        const name = cID; // The name/ID for the new entity.
        const taskKey = datastore.key([kind, name]); // The Cloud Datastore key for the new entity.
//============================================================
// Prepares the new entity.                                 ||
//============================================================
        const task =
            {
                key: taskKey,
                data: {
                    id: cID,
                    name: cProductName,
                    description: cProductDesc,
                    price: cProductPrice,
                    created_at: TimestampString,
                    updated_at: "-",
                },
            };
//========================================================================================================================
// Saves the entity and prints the details of the saved object in the console.                                          ||
//========================================================================================================================
        await datastore.save(task, (err, entity) => {
            if (err){
                return console.error(`Error: ` + err);
            } else {
                return console.log(`
                ----Saved----\n
                pID: ${cID}\n
                pName: ${task.key.name}\n
                pDesc: ${task.data.description}\n
                pPrice: ${task.data.price}\n
                -------------\n
            `   );
            }
        })
    },

//==================================================================================================================================
// This will be called and via the logic that is located here it will pass them to index.js and print them via the logic there.   ||
//==================================================================================================================================
    readAll: async function () {
        const query = datastore.createQuery(kind).order('created_at');
        const tasks = await datastore.runQuery(query);
        let allTasks = [];
        for (const task of tasks[0]) {
            allTasks.push(entity_to_obj(task));
        }
        if (allTasks.length === 0) {
            allTasks = [{"id":"-","status":"No data"}];
        }
        return allTasks;
    },

//=================================================================================================================================
// This will be called and via the logic that is located here it will pass them to index.js and print them via the logic there.  ||
//=================================================================================================================================
    read: async function (pID) {
        const query = datastore.createQuery(kind).filter('id', '=', pID);
        const tasks = await datastore.runQuery(query);
        let allTasks = [];
        for (const task of tasks[0]) {
            allTasks.push(entity_to_obj(task));
        }
        if (allTasks.length === 0) {
            allTasks = [{"id":"-","status":"No data"}];
        }
        return allTasks;
    },
//=================================================================================================================================
// This function will be called on PUT and will update a item to the value that is specified.                                    ||
//=================================================================================================================================
    update: async function (pID, toUpdate, valueForUpdate) {
        const name = pID;
        const whatToUpdate = toUpdate;
        const setValueUpdate = valueForUpdate;

        const update = datastore.transaction();
        const taskKey = datastore.key([kind, name]);
        // console.log(taskKey);
        // console.log(taskKey.path);
        try {
            await update.run();
            const [task] = await update.get(taskKey);
            task['updated_at'] = TimestampString;
            task[whatToUpdate] = setValueUpdate;
            update.save({
                key: taskKey,
                data: task,
            });
            await update.commit();
        } catch (err) {
            update.rollback();
            console.error(`${err}, Make sure you use product ID to update an entry`);
        }
        return console.log("Executed update function");
    },
//=================================================================================================================================
// This function will be called on DELETE and will delete the specified item.                                                    ||
//=================================================================================================================================
    delete: async function (pID) {
        const name = pID; // The name/ID from the entity.
        const taskKey = datastore.key([kind, name]); // The Cloud Datastore key for the removal of the entity.
        const task = await datastore.delete(taskKey);
        console.log(`${name} is successfully removed if it existed previously.`);
        return task;
    }
};