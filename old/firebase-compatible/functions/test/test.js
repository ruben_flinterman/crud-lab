//==============================================================
// Dependencies.                                              ||
//==============================================================
const request = require('supertest');
var should = require('chai').should();
//==============================================================
// Include index.js / functions.                              ||
//==============================================================
const path = require('path');
const core = require(path.resolve(__dirname, '../index.js'));
const Task = core.product;

//=================================================================================
// Function tests.                                                               ||
//=================================================================================
describe("Post Request(s)", () => {
    it('should post a new item', function (done) {
        const data = { id: "{id}", name: "{name}", desc: "{desc}", price: 999 };
        this.slow(1000);
        request(Task).post("/")
            .send(data)
            .expect(200)
            .expect('Content-Type', /json/)
            .end( (err, res) => {
                if (!err) {
                    res.body.should.be.a('object');
                    res.body.should.have.property("id");
                    res.body.should.have.property('name');
                    res.body.should.have.property('desc');
                    res.body.should.have.property('price');
                } else {
                    console.error(err);
                }
            });
        done();
    });
});

describe("Read Request(s)", () => {
    it('should request a json file that has ALL items in it.', function (done) {
        this.slow(1000);
        request(Task).get("/")
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(/created_at/)
            .expect(/name/)
            .expect(/description/)
            .expect(/price/, done);
    });

    it('should get a specific item', function (done) {
        const pName = '{id}';
        this.slow(1000);
        request(Task).get("/" + pName)
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(/created_at/)
            .expect(/name/)
            .expect(/description/)
            .expect(/price/, done);
    });
});

describe("Put Request(s)", () => {
    it('should update an entry', function (done) {
        const data = { id: "{id}", item: "price", value: "998" };
        this.slow(1000);
        request(Task).put("/")
            .send(data)
            .expect(200)
            .expect('Content-Type', /json/)
            .end( (err, res) => {
                if (!err) {
                    res.body.should.be.a('object');
                    res.body.should.have.property("id");
                    res.body.should.have.property('item');
                    res.body.should.have.property('value');
                } else {
                    console.error(err);
                }
            });
        done();
    });
});

describe("Delete Request(s)", () => {
    it('should delete an entry', function (done) {
        this.slow(1000);
        const data = { id: "{id}" };
        request(Task).delete("/" + data['id'])
            .expect(200)
            .expect('Content-Type', /json/, done)
    });
});

//============================================================
// Error tests.                                             ||
//============================================================
describe("Page requested that doesn't exist", () => {
    it('[POST] should give an error back', function (done) {
        this.slow(1000);
        request(Task).post("/page-doesnt-exist")
            .expect(404, done)
    });

    it('[PUT] should give an error back', function (done) {
        this.slow(1000);
        request(Task).put("/page-doesnt-exist")
            .expect(404, done)
    });
});

describe("403 error test - forbidden", () => {
    it('[POST] should give an error back', function (done) {
        this.slow(1000);
        request(Task).post("giveerror")
            .expect(403, done);
    });

    it('[GET] should give an error back', function (done) {
        this.slow(1000);
        request(Task).get("giveerror")
            .expect(403, done);
    });

    it('[PUT] should give an error back', function (done) {
        this.slow(1000);
        request(Task).put("giveerror")
            .expect(403, done)
    });

    it('[DELETE] should give an error back', function (done) {
        this.slow(1000);
        request(Task).delete("giveerror")
            .expect(403, done)
    });
});

describe("500 error tests - internal", () => {
    it('[POST] should give an error back', function (done) {
        const data = { 'none':'' };
        this.slow(1000);
        request(Task).post("/")
            .send(data)
            .expect(500)
            .expect('Content-Type', /json/, done)
    });
});