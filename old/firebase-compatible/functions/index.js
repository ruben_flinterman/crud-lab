//============================================================
// Dependencies.                                            ||
//============================================================
const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const fs = require('fs');
const path = require('path');
var bodyParser = require('body-parser');
//============================================================
// Swagger.                                                 ||
//============================================================
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
//==============================================================================
// Google service(s).                                                         ||
//==============================================================================
const gs = require(path.resolve(__dirname, './service-providers/google.js'));
//==============================================================================
// Config.                                                                    ||
//==============================================================================
const rawconfig = fs.readFileSync(path.resolve(__dirname, "config.json"));
const config = JSON.parse(rawconfig);
const solution = config.solution;
const region = config.region_functions;
//============================================================
// Error config.                                            ||
//============================================================
const solutionError = config.errors.solution_error;
//============================================================
// Initiate Express.                                        ||
//============================================================
const app = express();
app.use(bodyParser.json());
//============================================================
// Initiate Swagger.                                        ||
//============================================================
const swaggerOptions = {
    swaggerDefinition: {
        openapi: '3.0.0', // Specification (optional, defaults to swagger: '2.0'),
        info: {
            title: "Firebase-AWS-Azure",
            description: "Firebase-AWS-Azure Project Documentation",
            contact: {
                name: "Ruben Flinterman"
            },
            servers: ["http://localhost:5001"]
        }
    },
    // ['.routes/*.js']
    apis: ["index.js"]
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);
// expose the api doc in json format for external clients to use ...
app.get('/api-docs.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerDocs);
});

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

//========================================================================================================================================================
// Main.                                                                                                                                                ||
// The prefixes such as gs.* redirect to another file. The path of these files are                                                                      ||
// located right under (all) the dependencies in this file.                                                                                             ||
//                                                                                                                                                      ||
// Solutions are checked with if statements (as of now, may change later) and the variable solution which takes a value out of the config.json file.    ||
// When the value from the config.json file is equal to a valid if statement it will automatically start the right functions with the right SDK         ||
// Which are located in the files with the functions themselves. Everything in this files only calls and prints function (/ values).                    ||
//========================================================================================================================================================
if (solution === "google") {
    app.use(cors({origin: true}));
    /**
     * @swagger
     * /kynda-lab/europe-west1/product:
     *  post:
     *    description: On a post request with :id, :name, :desc & :price set the create function/query will be executed.
     *    responses:
     *      '200':
     *        description: A successful response
     *    consumes:
     *      - application/json
     *    parameters:
     *      - in: body
     *        name: productCreate
     *        description: Create a product
     *        schema:
     *          type: object
     *          required:
     *              - id
     *              - name
     *              - desc
     *              - price
     *          properties:
     *              id:
     *                  type: string
     *              name:
     *                  type: string
     *              desc:
     *                  type: string
     *              price:
     *                  type: integer
     *          example:
     *              id: "{id}"
     *              name: "{name}"
     *              desc: "{desc}"
     *              price: 999
     */
    app.post('/', async (req, res) => {
        console.log(req.body);
        const pID = req.body.id;
        const pName = req.body.name;
        const pDesc = req.body.desc;
        const pPrice = req.body.price;
        if (pID !== null || pID !== '' && pName !== null || pName !== '' && pDesc !== null || pDesc !== '' && pPrice !== null || pPrice !== '') {
            try {
                //Get specific result
                const readResult = await gs.read(pID);
                //Check for result
                if (readResult[0].id === pID) {
                    console.log("ID/entry already exists");
                    res.sendStatus(500);
                } else if (readResult[0].id === '-'){
                    gs.create(`${pID}`, `${pName}`, `${pDesc}`, `${pPrice}`);
                    res.send({id: pID, name: pName, desc: pDesc, price: pPrice, status: "created"});
                } else {
                    res.status(500).send("Error");
                }
            } catch (err) {
                console.error(err);
                res.status(500).send(err);
            }
        }
        return console.log("Create function executed");
    });

    /**
     * @swagger
     * /kynda-lab/europe-west1/product:
     *  get:
     *    description: When the googleGet function is called on root it will return ALL data from the database (in JSON format).
     *    responses:
     *      '200':
     *        description: A successful response
     */
    app.get('/', async (req, res) => {
        try {
            const result = await gs.readAll();
            return res.status(200).send(result);
        } catch (err) {
            return res.status(500).send(err);
        }
    });

    /**
     * @swagger
     * /kynda-lab/europe-west1/product/{productName}:
     *  get:
     *    description: When the googleGet function is called with a specific id (in this case the DB name) it will return all items which have the id as name.
     *    responses:
     *      '200':
     *        description: A successful response
     *    parameters:
     *        - in: path
     *          name: productName
     *          description: Get a product
     *          schema:
     *              type: string
     *              required:
     *                  - id
     */
    app.get('/:id', async (req, res) => {
        const pID = req.params.id;
        try {
            const result = await gs.read(pID);
            console.log(result);
            return res.status(200).send(result);
        } catch (err) {
            return res.status(500).send(err);
        }
    });

    /**
     * @swagger
     * /kynda-lab/europe-west1/product/:
     *  put:
     *    description: Update data that has the specified ID.
     *    responses:
     *      '200':
     *        description: A successful response
     *    parameters:
     *      - in: body
     *        name: productUpdate
     *        description: Create a product
     *        schema:
     *          type: object
     *          required:
     *              - id
     *              - item
     *              - value
     *          properties:
     *              id:
     *                  type: string
     *              item:
     *                  type: string
     *              value:
     *                  type: string
     *          example:
     *              id: "{id}"
     *              item: "price"
     *              value: "998"
     */
    app.put('/', async (req, res) => {
        console.log(req.body);
        const pID = req.body.id;
        const whatToUpdate = req.body.item;
        const changeTo = req.body.value;
        try {
            const result = await gs.update(pID, whatToUpdate, changeTo);
            console.log(result);
            return res.status(200).send({ id: pID, item: whatToUpdate, value: changeTo, status: "updated" });
        } catch (err) {
            console.error(`${err}, Make sure you use product ID to update an entry`);
            return res.status(500).send(err);
        }
    });

    /**
     * @swagger
     * /kynda-lab/europe-west1/product/{productDelete}:
     *  delete:
     *    description: Deleted data that has the specified ID.
     *    responses:
     *      '200':
     *        description: A successful response
     *    parameters:
     *        - in: path
     *          name: productDelete
     *          description: Delete a product
     *          schema:
     *              type: string
     *              required:
     *                  - id
     */
    app.delete('/:id', async (req, res) => {
        const pID = req.params.id;
        try {
            const result = await gs.delete(pID);
            console.log(result);
            return res.status(200).send({ id: pID, status: "removed" });
        } catch (err) {
            console.error(`${err}, Make sure you use product ID to remove an entry`);
            return res.status(500).send(err);
        }
    });

//===============================================================================================================================================
// Exports.                                                                                                                                    ||
// exports.{name} define the path the function will listen to.                                                                                 ||
// Ex: if it's exports.test and the function is called on root the function will be called with http:{domain}/test or http:{domain}/test/      ||
// The variables say which functions are going to be under which path.                                                                         ||
//===============================================================================================================================================
    exports.product = functions.region(region).https.onRequest(app);

} else if (solution === "aws") {
    // require aws SDK.
} else if (solution === "azure") {
    // require azure SDK.
} else {
    console.error(solutionError);
}