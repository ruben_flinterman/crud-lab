// Dependencies
const createHandler = require("azure-function-express").createHandler;
const express = require("express");
const path = require('path');
const crud = require(path.resolve(__dirname, './crud.js'));

// Create express app as usual
const app = express();
const functionPath = "/api/product";

// Actual code
app.post(`${functionPath}`, async(req, res) => {
    readResult = await crud.read(req.body);
    if (readResult.length == 0) {
        result = await crud.create(req.body);
    } else {
        result = { message: "Item already exists in the database."}
    }
    res.send(result);
});

app.get(`${functionPath}`, async(req, res) => {
    result = await crud.read(req.query);
    res.send(result)
});

app.put(`${functionPath}`, async(req, res) => {
    readResult = await crud.read(req.body);
    if (readResult.length == 0) {
        result = { message: "Cant update item since it doesnt exist"}
    } else {
        result = await crud.update(req.body);
    }
    res.send(result);
});

app.delete(`${functionPath}`, async(req, res) => {
    readResult = await crud.read(req.body);
    if (readResult.length == 0) {
        result = { message: "Cant delete item since it doesnt exist"}
    } else {
        result = await crud.remove(req.body);
    }
    res.send(result);
});

// Exports/Inits app
module.exports = createHandler(app);






// module.exports = async function (context, req) {
//     context.log('JavaScript HTTP trigger function processed a request.');

//     if (req.query.name || (req.body && req.body.name)) {
//         context.res = {
//             // status: 200, /* Defaults to 200 */
//             body: "Hello " + (req.query.name || req.body.name)
//         };
//     }
//     else {
//         context.res = {
//             status: 400,
//             body: "Please pass a name on the query string or in the request body"
//         };
//     }
// };