// Dependencies
const CosmosClient = require('@azure/cosmos').CosmosClient;
const path = require('path');
const fs = require('fs');
const rawconfig = fs.readFileSync(path.resolve(__dirname, "./config.json"));
const config = JSON.parse(rawconfig);

// Cosmos DB config
const endpoint = config.endpoint;
const key = config.key;
const databaseId = config.databaseId;
const containerId = config.containerId;

// Init Cosmos DB client
const client = new CosmosClient({ endpoint, key });

// Define database
const database = client.database(databaseId);
const container = database.container(containerId);

// Init default variables
let querySpec = {}
let category = undefined;

// https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-nodejs-get-started


module.exports = {
    // read: async function (passedData) {
    //     const pD = passedData;
    //     pD  .method = "GET";
    //     try {
    //         if (pD.name == null) { pD.name = "Not set" };
    //         const json = { hello: "world", test: pD.name, method:pD.method }
    //         return json
    //     } catch(err) {
    //         console.log(err);
    //     }
    // },

    read: async function (passedData) {
        const select = passedData;
        const setId = select.id;
        let resultArray = [];
        try {
            if (setId == null || typeof setId === 'undefined') { querySpec = { query: 'SELECT * from c'} } 
            else { querySpec = { query: `SELECT * from c WHERE c.id='${setId}'` };        }
            const { resources: results } = await container.items
                .query(querySpec)
                .fetchAll();
            for (let r of results) {
                resultArray.push({ id:r.id, name:r.name, desc:r.desc, price:r.price });
            };
            return resultArray;
        } catch (err) {
            console.log(err);
        };
    },

    create: async function (passedData) {
        const pD = passedData;
        let resultArray = [];
        try {
            newItem = { id:pD.id, name:pD.name, desc:pD.desc, price:pD.price };
            const { resource: createdItem } = await container.items.create(newItem);
            return newItem;
        } catch (err) {
            console.log(err);
        }
    },

    update: async function (passedData) {
        const pD = passedData;
        try {
            replaceData = { id:pD.id, name:pD.name, desc:pD.desc, price:pD.price };
            const { resource: itemToUpdate } = await container.item(pD.id, category).replace(replaceData);
            return replaceData;
        } catch (err) {
            console.log(err);
        }
    },

    remove: async function (passedData) {
        const pD = passedData;
        try {
            const { resource: result } = await container.item(pD.id, category).delete();
            removedItem = { message: `Removed item with id: ${pD.id}` };
            return removedItem;
        } catch (err) {
            console.log(err);
        }
    }
};