const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports = {
    read: async function () {
        return dynamoDb.scan({TableName: 'Product_test'}).promise()
            .then(response => response.Items)
    },

    readSpecific: async function (ItemId) {
        const params = {
            TableName: 'Product_test',
            Key: {
                "id": ItemId
            }
        };
        return dynamoDb.get(params).promise()
    },


    create: async function (request) {
        // return { message: "Test!" };
        const params = {
            TableName: "Product_test",
            Item: {
                id: request.body.id,
                name: request.body.name,
                desc: request.body.desc,
                price: request.body.price
            }
        };
        // if (typeof params.Item.id !== 'undefined' && typeof params.Item.name !== 'undefined' && typeof params.Item.desc !== 'undefined' && typeof params.Item.price !== 'undefined') {
            return dynamoDb.put(params).promise();
        // } else {
        //     return {message: "Not all variables are set.", params: params}
        // }
    },

    update: async function (request) {
        const params = {
            Key: {
                id: request.body.id
            },
            TableName: "Product_test",
            ConditionExpression: 'attribute_exists(id)',
            UpdateExpression: 'set ' + request.body.paramName + ' = :v',
            ExpressionAttributeValues: {
                ':v': request.body.paramValue
            },
            ReturnValue: 'ALL_NEW'
        };
        return dynamoDb.update(params).promise();
    },

    remove: async function (request) {
        const params = {
            Key: {
                id: request.body.id
            },
            TableName: 'Product_test'
        };
        return dynamoDb.delete(params).promise();
    }
};