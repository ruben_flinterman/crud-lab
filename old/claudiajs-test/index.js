const ApiBuilder = require('claudia-api-builder');
const api = new ApiBuilder();
const path = require('path');
const crud = require(path.resolve(__dirname, './crud.js'));

module.exports = api;

api.get('/', function () {
    return crud.read();
});

api.get('/{id}', function (request) {
    const ItemId = request.pathParams.id;
    return crud.readSpecific(ItemId);
});

api.post('/', async function (request) {
    const readResult = await crud.readSpecific(request.body.id);
    if (typeof readResult.Item === 'undefined') {
        return crud.create(request);
    } else {
        return { message:"An item with this ID already exists" };
    }
});

api.put('/', async function (request) {
    const readResult = await crud.readSpecific(request.body.id);
    if (typeof readResult.Item !== 'undefined') {
        return crud.update(request);
    } else {
        return { message:"There is no item with this ID." };
    }
});

api.delete('/', async function (request) {
    const readResult = await crud.readSpecific(request.body.id);
    if (typeof readResult.Item !== 'undefined') {
        return crud.remove(request);
    } else {
        return { message:"There is no item with this ID." };
    }
});

// claudia create --profile claudia --region eu-central-1 --api-module index --name crud-lab-test

