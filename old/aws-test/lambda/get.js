const AWS = require('aws-sdk');

exports.handler = async (event, context) => {
    const documentClient = new AWS.DynamoDB.DocumentClient();
    let stringID = toString(event.id);
    const params = {
        TableName: "Product_test",
        Key: {
            "id": event.id,
        }
    };

    try {
        const data = await documentClient.get(params).promise();
        console.log(event.id);
        console.log(`DATA: ${data}`);

        if (Object.entries(data).length === 0 || data === null) {
            return context.done(null, {status: "no data found"});
        }
        return context.done(null, data)
    } catch (err) {
        console.log(err);
    }
};