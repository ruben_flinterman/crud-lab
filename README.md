# Crud-Lab (Kynda)
# Project - Ruben Flinterman
---
Mijn (examen/stage)opdracht is het maken van een script die het verhuizen van een website tussen 
Firebase (Google), EC2 (AWS/Amazon) en Azure (Microsoft) makkelijker maakt. 
Dit project zou het maken van applicatie een stuk sneller moeten maken aangezien het verhuizen
korter wordt en je de code niet steeds hoeft te vervangen doordat het script zelf 'weet' op welk platform hij draait. (Deze gegevens zullen wellicht met een JSON bestand worden opgeslagen zodat het globaal kan worden opgehaald.) 
---
## **GET en POST requests:**
## **Firebase**

**Een entry aan de database toevoegen:**

POST: http://localhost:5001/kynda-lab/europe-west1/product/Desk_id/a_Desk_name/desk_desc/999

Placeholder(s):

```
{
	id: "{id}",
	name: "{name}",
	desc: "{desc}"
	price: 999
}
```

**Een specifiek item van de database ophalen:**

GET: http://localhost:5001/kynda-lab/europe-west1/product/Desk_id

Placeholder(s): http://localhost:5001/kynda-lab/europe-west1/product/{id}

**Alle items van de database ophalen:**

GET: http://localhost:5001/kynda-lab/europe-west1/product/

Placeholder(s): -

**Een specifiek item van de database updaten:**

PUT: http://localhost:5001/kynda-lab/europe-west1/product/Desk_id/price/998

Placeholder(s): 

```
{
	id: "{id}",
	item: "{what_to_update}",
	value: "{new_value}"
}
```

**Een specifiek item van de database verwijderen:**

DELETE: http://localhost:5001/kynda-lab/europe-west1/product/Desk_id

Placeholder(s): http://localhost:5001/kynda-lab/europe-west1/product/{id}

## **AWS**

**Een entry aan de database toevoegen:**

POST: https://2hbh2urb1g.execute-api.eu-central-1.amazonaws.com/latest

Placeholder(s): 

```
{
	"id": "{id}",
	"name": "{name}",
	"desc": "{desc}",
	"price": 999
}
```

**Een specifiek item van de database ophalen:**

GET: https://2hbh2urb1g.execute-api.eu-central-1.amazonaws.com/latest/1

Placeholder(s): https://2hbh2urb1g.execute-api.eu-central-1.amazonaws.com/latest/{id}

**Alle items van de database ophalen:**

GET: https://2hbh2urb1g.execute-api.eu-central-1.amazonaws.com/latest

**Een specifiek item van de database updaten:**

PUT: https://2hbh2urb1g.execute-api.eu-central-1.amazonaws.com/latest

Placeholder(s): 

```
{
	"id": "{id}",
	"paramName": "{item_to_update}",
	"paramValue": "{new_value}"
}
```

**Een specifiek item van de database verwijderen:**

DELETE: https://2hbh2urb1g.execute-api.eu-central-1.amazonaws.com/latest

Placeholder(s):

```
{
	"id": "{id}",
}
```

## **Azure**

**Een entry aan de database toevoegen:**

POST: http://localhost:7071/api/product

Placeholder(s): 

```
{
	id: "{id}",
	name: "{name}",
	desc: "{desc}"
	price: 999
}
```

**Een specifiek item van de database ophalen:**

GET: http://localhost:7071/api/product?id=1

Placeholder(s): http://localhost:7071/api/product?id={id}

**Alle items van de database ophalen:**

GET: http://localhost:7071/api/product

**Een specifiek item van de database updaten:**

PUT: http://localhost:7071/api/product

Placeholder(s): 

```
{
	id: "{id}",
	name: "{name}",
	desc: "{desc}"
	price: 999
}
```

**Een specifiek item van de database verwijderen:**

DELETE: http://localhost:7071/api/product

Placeholder(s): 

```
{
	id: "{id}",
}
```

---

## **Firebase Regions:**

us-central1 (Iowa)

us-east1 (South Carolina)

us-east4 (Northern Virginia)

europe-west1 (Belgium)

europe-west2 (London)

asia-east2 (Hong Kong)

asia-northeast1 (Tokyo)

## **AWS Regions:**

us-east-1 (US East (N. Virginia))

us-east-2 (US East (Ohio))

us-west-1 (US West (N. California))

us-west-2 (S West (Oregon))

ap-east-1 (Asia Pacific (Hong Kong))

ap-south-1 (Asia Pacific (Mumbai))

ap-northeast-1 (Asia Pacific (Tokyo))

ap-northeast-2 (Asia Pacific (Seoul))

ap-northeast-3 (Asia Pacific (Osaka-Local))  

ap-southeast-1 (Asia Pacific (Singapore))

ap-southeast-2 (Asia Pacific (Sydney))

ca-central-1 (Canada (Central))

eu-central-1 (Europe (Frankfurt))

eu-west-1 (Europe (Ireland))

eu-west-2 (Europe (London))

eu-west-3 (Europe (Paris))

eu-north-1 (Europe (Stockholm))

me-south-1 (Middle East (Bahrain))

sa-east-1 (South America (São Paulo))

## **Azure Regions:**

- https://www.azurespeed.com/Information/AzureRegions
- https://azure.microsoft.com/en-us/global-infrastructure/locations/
- https://azure.microsoft.com/en-us/global-infrastructure/geographies/