// https://www.pulumi.com/blog/code-deploy-and-manage-a-serverless-rest-api-on-aws-with-pulumi/

// Dependencies
const cloud = require("@pulumi/cloud-aws");

// Inits endpoints with specified name
const endpoint = new cloud.API("hello");

//// Sets output file (www/index.html)
// endpoint.static("/", "www");

// (Endpoint) Handlers
const JsonData = { message: "AWS 🕹️" };
endpoint.get("/source", (req, res) => res.json(JsonData));

// Export function(s)
exports.url = endpoint.publish().url;




// Old code:

// // Import the [pulumi/aws](https://pulumi.io/reference/pkg/nodejs/@pulumi/aws/index.html) package
// const pulumi = require("@pulumi/pulumi");
// const aws = require("@pulumi/aws");
// const awsx = require("@pulumi/awsx");
//
// const testJsonData = { message: "AWS 🕹️" };
//
// // Create a public HTTP endpoint (using AWS APIGateway)
// const endpoint = new awsx.apigateway.API("hello", {
//     routes: [
//         // Serve static files from the `www` folder (using AWS S3)
//         {
//             path: "/",
//             localPath: "www",
//         },
//
//         // Serve a simple REST API on `GET /name` (using AWS Lambda)
//         {
//             path: "/source",
//             method: "GET",
//             eventHandler: (req, ctx, cb) => {
//                 cb(undefined, {
//                     statusCode: 200,
//                     // body: Buffer.from(JSON.stringify({ name: "AWS" }), "utf8").toString("base64"),
//                     body: Buffer.from(JSON.stringify(testJsonData), "utf8").toString("base64"),
//                     isBase64Encoded: true,
//                     headers: { "content-type": "application/json" },
//                 });
//             },
//         },
//     ],
// });
//
// // Export the public URL for the HTTP service
// exports.url = endpoint.url;