// https://github.com/pulumi/examples/blob/master/gcp-ts-functions/index.ts
// https://stackoverflow.com/questions/47511677/firebase-cloud-function-your-client-does-not-have-permission-to-get-url-200-fr

// Dependencies
// const gcp = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");
const JsonData = { message: "Hello Ruben! 🤖" };
// gcp.serviceAccount.getAccount({
//     accountId: "",
//     displayName: ""
// });

// (Endpoint) Handlers
const greeting = new gcp.cloudfunctions.HttpCallbackFunction("greeting", ((req, res) => {
    res.send(JsonData);
}));

// Export function(s)
exports.url = greeting.httpsTriggerUrl;








// Old code:

// "use strict";
// const pulumi = require("@pulumi/pulumi");
// const gcp = require("@pulumi/gcp");
//
// // Create a GCP resource (Storage Bucket)
// const bucket = new gcp.storage.Bucket("pulumi-test");
//
// // Export the DNS name of the bucket
// exports.bucketName = bucket.url;