// Dependencies
const gcp = require("@pulumi/gcp");
// const path = require('path');


const endpoint = new gcp.cloudfunctions.HttpCallbackFunction("pulumi-test", {
    callbackFactory: () => {
        // Require express
        const express = require('express');
        const bodyParser = require("body-parser");
        // Default variables
        const JsonData = {message: "Hello Ruben! 🤖"};
        const gs = require('./crud.js');

        const app = express();
        app.use(bodyParser.json());
        app.get("/", GETAllRequest);

        async function GETAllRequest(req, res) {
            res.send(JsonData);
        }
        
        return app;
    }
});

// Export function(s)
exports.url = endpoint.httpsTriggerUrl;