const { exec } = require("child_process");

module.exports = {
    setup: async function () {
        try {
            exec("cd solutions/gcp && Pulumi.dev.yaml stack select dev && Pulumi.dev.yaml up", (error, stdout, stderr) => {
                if (error) {
                    console.log(`error: ${error.message}`);
                    return;
                }
                if (stderr) {
                    console.log(`stderr: ${stderr}`);
                    return;
                }
                console.log(`stdout: ${stdout}`);
            });
        } catch (err) {
            return console.error(err);
        }
    }
};