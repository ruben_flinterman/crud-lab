module.exports = {
    setup: async function () {
        try {
            msg = 'Done';
            error = {message: msg};
            return error;
        } catch (err) {
            return console.error(err);
        }
    }
};