// Dependencies
const cloud = require("@pulumi/cloud-aws");

// Inits endpoints with specified name
const endpoint = new cloud.API("hello");

//// Sets output file (www/index.html)
// endpoint.static("/", "www");

// (Endpoint) Handlers
const JsonData = { message: "AWS 🕹️" };
endpoint.get("/", (req, res) => res.json(JsonData));

// Export function(s)
exports.url = endpoint.publish().url;