// Dependencies.
const fs = require('fs');
const path = require('path');

// Config
const rawconfig = fs.readFileSync(path.resolve(__dirname, "./config.json"));
const config = JSON.parse(rawconfig);
const solution = config.solution;

const error = require(path.resolve(__dirname, './errors/errors.js'));

if (solution === 'gcp' || solution === 'aws' || solution === 'azure') {
    const deploy = require(path.resolve(__dirname, `./solutions/${solution}/deploy.js`));
    deploy.setup();
} else {
        return console.log(error.solutionDoesntExist());
}